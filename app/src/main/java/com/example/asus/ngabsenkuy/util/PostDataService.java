package com.example.asus.ngabsenkuy.util;

import com.example.asus.ngabsenkuy.model.ResponseSekolah;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface PostDataService {

    @FormUrlEncoded
    @POST("registerAdmin.php")
    Call<ResponseBody> registerAdmin(
            @Field("nama") String nama,
            @Field("email") String email,
            @Field("pass") String pass,
            @Field("tlp") String telp,
            @Field("gender") String gender,
            @Field("sekolah") String sekolah
    );

    @FormUrlEncoded
    @POST("registerGuru.php")
    Call<ResponseBody> registerGuru(
            @Field("nama") String nama,
            @Field("email") String email,
            @Field("pass") String pass,
            @Field("tlp") String telp,
            @Field("gender") String gender,
            @Field("sekolah") String sekolah
    );

    @FormUrlEncoded
    @POST("login.php")
    Call<ResponseBody> login(
            @Field("email") String email,
            @Field("pass") String pass
    );

    @GET("sekolah.php")
    Call<ResponseSekolah> getSekolah();

}

