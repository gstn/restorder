package com.example.asus.ngabsenkuy.ui.auth;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.asus.ngabsenkuy.MainActivity;
import com.example.asus.ngabsenkuy.R;
import com.example.asus.ngabsenkuy.api.APIUtil;
import com.example.asus.ngabsenkuy.util.PostDataService;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = LoginActivity.class.getSimpleName();

    private TextView txtDisini;
    private EditText edtEmail, edtPassword;
    private Button btnLogin;

    private String email, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initLayout();
        btnMasukClicked();
        txtDisiniClicked();
    }

    private void txtDisiniClicked() {
        txtDisini.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegisterAdminActivity.class));
            }
        });
    }

    private void btnMasukClicked() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getInputValues();

                PostDataService postDataService = APIUtil.builder().create(PostDataService.class);
                Call<ResponseBody> call = postDataService.login(email, password);
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                        Log.i(TAG, "onResponse: login : " + response.message());
                        Intent loginIntent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(loginIntent);
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                        Log.e(TAG, "onFailure: login :" + t.getMessage() + "\n" + t.getCause());
                    }
                });
            }
        });
    }

    private void getInputValues() {
        email = edtEmail.getText().toString();
        password = edtPassword.getText().toString();
    }

    private void initLayout() {
        edtEmail = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtPass);
        btnLogin = findViewById(R.id.btnMasuk);
        txtDisini = findViewById(R.id.txtDisini);
    }
}
