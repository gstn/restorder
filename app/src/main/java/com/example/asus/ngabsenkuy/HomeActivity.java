package com.example.asus.ngabsenkuy;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.asus.ngabsenkuy.ui.auth.LoginActivity;
import com.example.asus.ngabsenkuy.ui.auth.RegisterAdminActivity;
import com.example.asus.ngabsenkuy.ui.auth.RegisterGuruActivity;

public class HomeActivity extends AppCompatActivity {

    private Button btnDaftar, btnMasuk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        initLayout();

        btnMasukClicked();
        btnDaftarClicked();
    }

    private void btnDaftarClicked() {
        btnDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog = new Dialog(HomeActivity.this);
                dialog.setContentView(R.layout.dialog_register);

                dialog.findViewById(R.id.btnGuru).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(HomeActivity.this, RegisterGuruActivity.class));
                    }
                });

                dialog.findViewById(R.id.btnAdmin).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(HomeActivity.this, RegisterAdminActivity.class));
                    }
                });

                dialog.findViewById(R.id.btnSiswa).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent());
                    }
                });
                dialog.show();

            }
        });
    }

    private void btnMasukClicked() {
        btnMasuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, LoginActivity.class));
            }
        });
    }

    private void initLayout() {
        btnDaftar = findViewById(R.id.btnDaftar);
        btnMasuk = findViewById(R.id.btnMasuk);
    }
}
