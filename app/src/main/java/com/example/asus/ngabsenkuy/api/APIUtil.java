package com.example.asus.ngabsenkuy.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIUtil {

    private static final String URL = "http://192.168.8.104/web/ngabsen_kuy/";
    private static Retrofit retrofit;

    public static Retrofit builder() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
