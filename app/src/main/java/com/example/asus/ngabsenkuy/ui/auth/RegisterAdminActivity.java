package com.example.asus.ngabsenkuy.ui.auth;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.asus.ngabsenkuy.R;
import com.example.asus.ngabsenkuy.api.APIUtil;
import com.example.asus.ngabsenkuy.util.PostDataService;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterAdminActivity extends AppCompatActivity {

    private static final String TAG = RegisterAdminActivity.class.getSimpleName();

    private TextView txtDisini;
    private EditText edtNama, edtEmail, edtPassword, edtSekolah, edtTelepon;
    private RadioGroup radioGroup;
    private Button btnDaftar;

    private String nama, email, pass, sekolah, telepon, gender;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_admin);

        initLayout();

        btnDaftarClicked();
        txtDisiniClicked();
    }

    private void txtDisiniClicked() {
        txtDisini.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegisterAdminActivity.this, LoginActivity.class));
            }
        });
    }

    private void btnDaftarClicked() {
        btnDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getInputValues();

                Log.i(TAG, "onClick: " + nama + "\n" + email + "\n" + pass + "\n" + telepon + "\n" + gender);
                sendPostData(nama, email, pass, telepon, gender, sekolah);

            }
        });
    }

    private void sendPostData(String nama, String email, String pass, String phone, String gender, String sekolah) {
        PostDataService postDataService = APIUtil.builder().create(PostDataService.class);
        Call<ResponseBody> call = postDataService.registerAdmin(
                nama,
                email,
                pass,
                phone,
                gender,
                sekolah
        );

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                Log.i(TAG, "onResponse: login : " + response.message());
                Intent loginIntent = new Intent(RegisterAdminActivity.this, LoginActivity.class);
                startActivity(loginIntent);
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure: login :" + t.getMessage() + "\n" + t.getCause());
            }
        });
    }

    private void getInputValues() {
        nama = edtNama.getText().toString();
        email = edtEmail.getText().toString();
        pass = edtPassword.getText().toString();
        sekolah = edtSekolah.getText().toString();
        telepon = edtTelepon.getText().toString();

        int radioId = radioGroup.getCheckedRadioButtonId();
        RadioButton radioButton = findViewById(radioId);
        gender = radioButton.getText().toString();
        if (gender.equals("Laki-Laki")) {
            gender = "1";
        } else {
            gender = "2";
        }
    }

    private void initLayout() {
        txtDisini = findViewById(R.id.txtDisini);
        edtNama = findViewById(R.id.edtNama);
        edtEmail = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtPass);
        edtSekolah = findViewById(R.id.edtInstansi);
        edtTelepon = findViewById(R.id.edtTelepon);
        radioGroup = findViewById(R.id.radioGroup);
        btnDaftar = findViewById(R.id.btnDaftar);
    }

}
