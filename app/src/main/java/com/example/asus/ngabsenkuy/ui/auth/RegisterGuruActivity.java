package com.example.asus.ngabsenkuy.ui.auth;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.asus.ngabsenkuy.R;
import com.example.asus.ngabsenkuy.api.APIUtil;
import com.example.asus.ngabsenkuy.model.DataItem;
import com.example.asus.ngabsenkuy.model.ResponseSekolah;
import com.example.asus.ngabsenkuy.util.PostDataService;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterGuruActivity extends AppCompatActivity {

    private static final String TAG = RegisterGuruActivity.class.getSimpleName();

    private PostDataService postDataService;

    private EditText edtNama, edtEmail, edtPass, edtTelepon;
    private RadioGroup radioGroup;
    private Spinner spSekolah;
    private Button btnDaftar;

    private String nama, email, pass, tlp, gender, sekolah;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_guru);

        postDataService = APIUtil.builder().create(PostDataService.class);

        initLayout();
        getSpinnerValues();
        btnDaftarClicked();
    }

    private void getSpinnerValues() {
        postDataService.getSekolah().enqueue(new Callback<ResponseSekolah>() {
            @Override
            public void onResponse(Call<ResponseSekolah> call, Response<ResponseSekolah> response) {
                if (response.isSuccessful()) {
                    List<DataItem> dataItemsList = response.body().getListSekolah();
                    List<String> listSpinner = new ArrayList<>();
                    for (int i = 0; i < dataItemsList.size(); i++) {
                        listSpinner.add(dataItemsList.get(i).getNamaSekolah());
                    }
                    ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>
                            (RegisterGuruActivity.this, android.R.layout.simple_spinner_item, listSpinner);
                    spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spSekolah.setAdapter(spinnerAdapter);
                }
            }

            @Override
            public void onFailure(Call<ResponseSekolah> call, Throwable t) {
                Log.i(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    private void btnDaftarClicked() {
        btnDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getInputValues();

                postDataService.registerGuru(nama, email, pass, tlp, gender, sekolah).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Log.i(TAG, "onResponse: Registr Guru : " + response.message());
                        Toast.makeText(RegisterGuruActivity.this, "success", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(RegisterGuruActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                        Log.i(TAG, "onFailure: RegisterGuru : " + t.getMessage() + "\n" + t.getCause());
                    }
                });

            }
        });
    }

    private void getInputValues() {
        nama = edtNama.getText().toString();
        email = edtEmail.getText().toString();
        pass = edtPass.getText().toString();
        tlp = edtTelepon.getText().toString();

        int id = radioGroup.getCheckedRadioButtonId();
        RadioButton radioButton = findViewById(id);
        gender = radioButton.getText().toString();

        spSekolah.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sekolah = Objects.requireNonNull(parent.getOnItemSelectedListener()).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void initLayout() {
        edtNama = findViewById(R.id.edtNama);
        edtEmail = findViewById(R.id.edtEmail);
        edtPass = findViewById(R.id.edtPass);
        edtTelepon = findViewById(R.id.edtTelepon);
        radioGroup = findViewById(R.id.radioGroup);
        spSekolah = findViewById(R.id.spSekolah);
        btnDaftar = findViewById(R.id.btnDaftar);
    }
}
